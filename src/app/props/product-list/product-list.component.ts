import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  @Input() danhSachSanPham: any[] = [];
  thongTinSanPham: any = {};

  constructor() { }

  xemChiTiet(sp: any): void {
    // sp vai trò là event nhận từ component con ra
    // Lấy sp được click gán cho cha thông qua thongTinSanPham => giao diện render lại
    this.thongTinSanPham = sp;
  }
  
  ngOnInit(): void {
  }

}
