import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoInputComponent } from './demo-input/demo-input.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductListComponent } from './product-list/product-list.component';



@NgModule({
  declarations: [DemoInputComponent, ProductItemComponent, ProductListComponent],
  imports: [
    CommonModule
  ],
  exports: [
    DemoInputComponent
  ]
})
export class PropsModule { }
