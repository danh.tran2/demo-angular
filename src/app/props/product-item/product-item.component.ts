import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent implements OnInit {
  @Input() sp: any = {};

  // import tay EventEmitter vì auto import sai lib
  @Output() eventXemChiTiet = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  xemChiTiet(): void {
    //Dùng eventXemChiTiet để trả giá trị về component cha (Nơi chứa giao diện modal)
    //Emit (value): value là giá trị trả ra cho component cha ứng với $event
    this.eventXemChiTiet.emit(this.sp);
  }
}
