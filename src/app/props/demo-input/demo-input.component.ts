import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductListComponent } from '../product-list/product-list.component';

@Component({
  selector: 'app-demo-input',
  templateUrl: './demo-input.component.html',
  styleUrls: ['./demo-input.component.scss']
})
export class DemoInputComponent implements OnInit {

  constructor() { }

  public dsSanPham:any[] = [
    {ma:1,tenSP:'Xperia xz 1',gia:1000},
    {ma:2,tenSP:'Iphone X',gia:2000},
    {ma:3,tenSP:'Samsung note 10+',gia:3000}
  ]

  ngOnInit(): void {
  }

  @ViewChild('tagProductList')
  tagProductList: ProductListComponent;

  themSanPham(ma:string, tenSP:string, gia:string){
    // this.dsSanPham.push({ma,tenSP,gia})
    // cách 1: View -> push vào model, model re-render lại view --> mô hình MVVC
    // Cách 2: Dùng ViewChild để truy cập vào nội dung bên trong 1 tag được đánh dấu
    // DOm đến thẻ <app-product-list>: => thay đổi thuộc tính danhSachSanPham => component <app-product-list>
    // </app-product-list> sẽ tự động render lại
    this.tagProductList.danhSachSanPham.push({ma,tenSP,gia})
  }

}
