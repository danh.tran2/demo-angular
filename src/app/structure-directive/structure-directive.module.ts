import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StructureDirectiveComponent } from './structure-directive.component';
import { NgifComponent } from './ngif/ngif.component';
import { NgforComponent } from './ngfor/ngfor.component';
import { NgswitchComponent } from './ngswitch/ngswitch.component';



@NgModule({
  declarations: [StructureDirectiveComponent, NgifComponent, NgforComponent, NgswitchComponent],
  exports: [StructureDirectiveComponent], 
  imports: [
    CommonModule
  ]
})
export class StructureDirectiveModule { }
