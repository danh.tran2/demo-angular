import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.scss']
})
export class NgforComponent implements OnInit {
  // hoặc object[] / any
  danhSachNhanVien: Array<Object> = [
    {ten: "Nguyen", tuoi: 18},
    {ten: "Khoa", tuoi: 19},
    {ten: "Duy", tuoi: 20},
    {ten: "Dung", tuoi: 21},
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
