import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  a = 'one way binding';
  name = 'two way binding';
  constructor() {}

  ngOnInit(): void {}

  tinhTong() {
    console.log(3);
  }
}
