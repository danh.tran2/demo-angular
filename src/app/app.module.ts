import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { AdminModule } from './admin/admin.module';
import { StructureDirectiveModule } from './structure-directive/structure-directive.module';
import { AttributeDirectiveModule } from './attribute-directive/attribute-directive.module';
import { PropsModule } from './props/props.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentComponent,
    SidebarComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    StructureDirectiveModule,
    AttributeDirectiveModule,
    PropsModule //Module chứa thẻ Demo-input
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
