import { Directive, HostBinding, HostListener, ElementRef, Renderer2, } from '@angular/core';

@Directive({
  selector: '[appHighLight]'
})
export class HighLightDirective {

  constructor(private ele:ElementRef, private render2: Renderer2) { 
    // có 2 cách - dùng lib render2 hoặc tự set ele luôn
    // this.ele.nativeElement.style.backgroundColor = "red"
    this.render2.setStyle(this.ele.nativeElement, "background-color", "red");
  }
  @HostBinding("style.backgroundColor") bgColor: string = "aqua";

  @HostListener("mouseenter") SuKienHover() {
    this.ele.nativeElement.style.backgroundColor = "green"
    // this.render2.setStyle(this.ele.nativeElement, "background-color", "yellow");
  }

  @HostListener("mouseleave") SuKienLeave() {
    this.render2.setStyle(this.ele.nativeElement, "background-color", "blue");
  }

}
